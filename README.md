*Copyright 2002 Joaquin Moreno Guzmán
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.*

# README #

### Para que sirve este repositorio ###

* El objetivo de este repositorio es obtener una aproximacio del numero pi a partir del metodo de Montecarlo
* Version Final

### Como se usa ###

Para ejecutar el programa use el comando "java -jar principal.jar numeroDePuntosAUsar.Para generar el archivo .jar ejecute el comando make run. Cuanto mayor sea el numero de puntos que utilice mayor sera la precision del numero pi.

### Dueño del repositorio ###

* Joaquín Moreno Guzmán
~                          