package aplicacion;
import mates .*;
/**
 * Esta clase contiene el método main que ejecuta el programa e imprime el numero pi
 *
 * @author Joaquin Moreno Guzman
 *
 * @version 01-03-2021
 */
public class Principal{
	/**
	 * El método main sirve para imprimir la estimacion de pi calculada
	 *
	 * @param args es el valor que asigna el usuario
	 */
	public static void main(String args[]){
		System.out.println("El número PI es " + Matematicas.generarNumeroPi(Integer.parseInt(args[0])));
	}
}
