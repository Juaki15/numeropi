package mates;
import java.lang.Math;

/**
 *Esta clase contiene mi método "generarNumeroPi"
 *@author Joaquín Moreno Guzmán
 *@version 01-03-2021
*/
public class Matematicas{
	/**
	 * El método generarNumeroPi obtiene una estimacion del numero pi segun el metodo de Montecarlo
	 *
	 * @param pasos es el número de puntos que se generan
	 */
	static int aciertos = 0;
	static double areaCuadrado = 4;
      	public static double generarNumeroPi(long pasos){
		double aciertos = 0;
	       	long numeroMaximo = pasos ;
		for (int i=1; i < numeroMaximo; i++){
			double x = (Math.random()*2-1);
		       	double y = (Math.random()*2-1);
			if (Math.sqrt((x*x) + (y*y)) <=1){
				aciertos = aciertos + 1;
			} 	
		}
		int radio = 1;
		double areaCirculo = areaCuadrado*(aciertos/numeroMaximo);
		double estimacionPi = areaCirculo/(radio*radio);
		return estimacionPi;	
	}
}

